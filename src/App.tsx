import * as React from "react";
import * as CMP from "./components";
import { Box, Button } from "@material-ui/core";

interface StateProvider {
  contacts: any,
  items: Array<{}>,
  contactsCount: number,
  isEdit: boolean
}
export class App extends React.Component<{}, StateProvider>{
  constructor(props) {
    super(props);
    this.state = {
      contacts: {
        name: "",
        address: ""
      },
      items: [],
      contactsCount: 0,
      isEdit: false,

    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.enableEdit = this.enableEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  };

  onChange = (event) => {
    event.preventDefault();
    const name = event.target.name;
    const value = event.target.value;
    const { contacts } = this.state;
    const newContact = {
      ...contacts, [name]: value
    };
    this.setState({
      contacts: newContact
    });
  };

  updateItems = () => {
    this.setState((state) => ({
      items: [...state.items, state.contacts],
      contactsCount: state.contactsCount + 1
    }))
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.onChange(event);
    if (!this.state.contacts.name || !this.state.contacts.address) {
      return
    }
    else {
      this.updateItems();
      this.setState({
        contacts: {
          name: "",
          address: ""
        }
      });
    }
  };

  handleDelete(itemToBeDeleted) {
    const contactsCount = this.state.contactsCount;
    var newContacts = this.state.items.filter((item) => {
      return item !== itemToBeDeleted
    });

    this.setState({
      items: newContacts,
      contactsCount: contactsCount - 1
    });
  };

  enableEdit(event) {
    event.preventDefault();
    if (this.state.items.length > 0) {
      this.setState({ isEdit: true })
    }

    if (this.state.isEdit) {
      this.setState({ isEdit: false })
    }
  };

  editContact(event) {
    // event.preventDefault();
    // const name = event.target.name;
    // const value = event.target.value;
    // const { contacts } = this.state;
    // const newContact = {
    //   ...contacts, [name]: value
    // }

    // this.setState((state) => ({
    //   contacts: newContact,
    //   items: [Object.assign(state.items[state.items.findIndex(el => el.id === contacts.id)], contacts)]
    // }));
  }


  render() {
    const editText = this.state.isEdit ? "Done" : "Edit"
    return (

      <>
        <Box className="contact-app">
          <CMP.SignForm contacts={this.state.contacts} onChange={this.onChange} onSubmit={this.onSubmit} />
          <div className="contact-features">
            <div className="contacts-count">{`Contacts Count (${this.state.contactsCount})`}</div>
            <Button variant="contained" className="contact-edit-button" onClick={this.enableEdit}>{editText}</Button>
          </div>
          <CMP.Contacts items={this.state.items} onClickDelete={this.handleDelete} editable={this.state.isEdit} onDClick={this.editContact.bind(this)} />
        </Box>
      </>
    );
  }
}
export default App