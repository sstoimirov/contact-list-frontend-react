import * as React from "react";
import { TextField, Button, FormControl } from "@material-ui/core";

type SignFormProps = {
    contacts: any,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    onSubmit: (e: React.MouseEvent<HTMLFormElement>) => void
}
export class SignForm extends React.PureComponent<SignFormProps, {}>{
    render() {
        const {contacts,onChange,onSubmit} = this.props

        return (
            <form className="contact-form" onSubmit={onSubmit}>
                <FormControl className="contact-form__input">
                    <TextField id="outlined-name" label="Name" name="name" margin="normal" variant="outlined" onChange={onChange} value={contacts.name}/>
                </FormControl>
                <FormControl className="contact-form__input">
                    <TextField id="outlined-email" label="Email" name="address" margin="normal" variant="outlined" type="email" onChange={onChange} value={contacts.address}/>
                </FormControl>
                <Button className="submit-button" variant="contained" color="primary" type="submit">Submit</Button>
            </form>
        )
    }
}