import * as React from "react";
import { SignForm } from "./SignForm";
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe("Contact", () => {
    let contacts, items, fn
    beforeAll(() => {
        contacts = {
            name: "Jon Doe",
            email: "jondoe@gmail.com"
        }
        items = [contacts]
        fn = jest.fn();
    })
    it("Render Contact with all props", () => {
        const comp = mount(<SignForm contacts={contacts} items={items} onChange={fn} onSubmit={fn} />);
        expect(comp.debug()).toMatchSnapshot();
    })
})