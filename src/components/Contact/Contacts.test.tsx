import * as React from "react";
import { Contact } from "./Contact";
import { configure,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe("Contact", () => {
    let id, name, email, fn
    beforeAll(() => {
        id = "1";
        name = "Jon Doe";
        email = "jondoe@gmail.com";
        fn = jest.fn();
    })
    it("Render Contact with all props", () => {
        const comp = mount(<Contact id={id} name={name} email={email} onDClick={fn} onClickDelete={fn} />);
        expect(comp.debug()).toMatchSnapshot();
    })
})