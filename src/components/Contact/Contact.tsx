import * as React from "react";
import { TextField, Button, FormControl, Avatar, Box } from "@material-ui/core";
import MailIcon from '@material-ui/icons/Mail';

type ContactProps = {
    id: string
    name: string,
    email: string,
    isDisabled?: boolean,
    onClickDelete: (e: React.MouseEvent<HTMLButtonElement>) => void,
    onDClick: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
};

export class Contact extends React.PureComponent<ContactProps, {}>{

    render() {
        const { name, email, isDisabled, onClickDelete, id } = this.props;
        const avatar = name.charAt(0);
        return (
            <>
                <form id={id}>
                    <Box className="contact-box">
                        <Box className="contact contact-name">
                            <Avatar className="avatar">{avatar}</Avatar>
                            <FormControl className="contact-text">
                                <TextField onDoubleClick={this.props.onDClick} margin="normal" name="name" type="text" value={name} variant="outlined" disabled={!isDisabled} />
                            </FormControl>
                        </Box>
                        <Box className="contact contact-email">
                            <Avatar className="avatar">
                                <MailIcon />
                            </Avatar>
                            <FormControl className="contact-text">
                                <TextField onDoubleClick={this.props.onDClick} margin="normal" name="address" type="text" value={email} variant="outlined" disabled={!isDisabled} />
                            </FormControl>
                        </Box>
                        <Button className="contact-delete-btn" variant="contained" color="secondary" disabled={!isDisabled} onClick={onClickDelete}>{"X"}</Button>
                    </Box>
                </form>
            </>
        )
    }
}