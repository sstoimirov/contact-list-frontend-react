import * as React from "react";
import {Contacts} from "./Contacts";
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe("Contacts", () => {
    it("render Contacts without elements", () => {
        const comp = mount(<Contacts items={[]} onClickDelete={jest.fn()} onDClick={jest.fn} editable={false}/>)
        expect(comp.debug()).toMatchSnapshot();
    })

    it("render Contacts with elements", () => {
        const el = {
            name: "Jon Doe",
            email: "jondoe@gmail.com"
        }
        const comp = mount(<Contacts items={[el]} onClickDelete={jest.fn()} onDClick={jest.fn} editable={true}/>)
        expect(comp.debug()).toMatchSnapshot();
        expect(comp).toHaveLength(1)
    })

    it("check Contacts length", () => {
        const el = {
            name: "Jon Doe",
            email: "jondoe@gmail.com"
        }
        const comp = mount(<Contacts items={[el]} onClickDelete={jest.fn()} onDClick={jest.fn} editable={true}/>)
        expect(comp).toHaveLength(1)
    })
})