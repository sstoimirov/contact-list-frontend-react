import * as React from "react";
import { Contact } from "../Contact";

type ContactsProps = {
    items: any,
    onClickDelete: (e: React.MouseEvent<HTMLDivElement>) => void,
    editable: boolean,
    onDClick: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
}
export class Contacts extends React.Component<ContactsProps, {}>{
    render() {
        const { items, onClickDelete, editable, onDClick } = this.props;
        return (
            <div className="contacts-wrapper" data-editable={editable}>
                {items.map((item, index) =>
                    <Contact
                        key={index}
                        id={`contact_${index}`}
                        name={`${item.name}`}
                        email={`${item.address}`}
                        onClickDelete={onClickDelete.bind(this, item)}
                        isDisabled={editable}
                        onDClick={onDClick.bind(this)}
                    />
                )}
            </div>
        )
    }
}